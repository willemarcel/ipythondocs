# -*- coding: utf-8 -*-
#
# Copyright (c) 2013
# Danilo de Jesus da Silva Bellini and Wille Marcel Lima Malheiro
#
# This is free software (MIT license). See LICENSE.txt for more details.
# Created on Sat Oct 05 2013
""" A py.test pluging to discover/run IPython-style tests in docstrings. """

def pytest_addoption(parser):
    group = parser.getgroup("collect")
    group.addoption("--ipythondocs",
        action="store_true", default=False,
        help="run IPython docstring tests in all .py modules",
        dest="ipythondocs")

